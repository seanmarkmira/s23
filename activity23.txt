Create a hotel database
 - Right click on `New Connection` and click `Create Database`
 - Name the database as `hotel`

 Create a database of hotel with a collection of rooms
 - Issue command `db.createCollection("rooms")

 Insert a single room (insertOne method) with the following details:
 1. Name - single
 2. Accomodates - 2
 3. price - 1000
 4. description - A simple room with all the basic necessities
 5. rooms_available - 10
 6. isAvailable - false

 - Issue command below:
 	db.rooms.insertOne({"name":"single", "accomodates":2,"price":1000,"description":"A simple room with all the basic necessities", "rooms_available":10, "is_available":false})

Insert multiple rooms (insertMany method) with the following details
1. name - double
2. accomodates - 3
3. price - 2000
4. description - A room fit for a small family going on a vacation
5. rooms_available - 5
6. isAvailable - false

 - Issue command below:
 	db.rooms.insertMany([{"name":"double", "accomodates":3,"price":2000,"description":"A room fit for a small family going on a vacation", "rooms_available":5, "is_available":false}, {"name":"presidential", "accomodates":10,"price":45000,"description":"A room for VIP/Politicians/Artists/etc", "rooms_available":3, "is_available":false}])

Use the find method to search for a room with the name double

 - Issue command below:
 	db.rooms.find({"name":"double"})


Use the updateOne method to update the queen room and set the available rooms to 0

 - Issue command below:
 	db.rooms.updateOne({"name":"queen"},{$set:{"rooms_available":0}})

Use the deleteMany method rooms to delete all rooms that have 0 availability

 - Issue command below:
 	db.rooms.deleteMany({"rooms_available":{$eq:0}})